//
//  ViewController.swift
//  SampleApp
//
//  Created by leandroramosss on 02/15/2020.
//  Copyright (c) 2020 leandroramosss. All rights reserved.
//

import UIKit
import SnapKit
import SampleApp

protocol ViewLayout {
    func addSubView()
    func constraintsMaker()
    func additionalLayout()
}

extension ViewLayout {
    func ManageBackGround() {
    }
    
    func addActions() {
    }
}

class ViewController: UIViewController {
    
    lazy var safetyContainer: UIView = {
       return UIView()
    }()
    
    lazy var startButton: UIButton = {
        return UIButton()
    }()
    
//    override func viewWillLayoutSubviews() {
//        addActions()
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        addSubView()
        constraintsMaker()
        additionalLayout()
        ManageBackGround()
        addActions()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension ViewController: ViewLayout {
    func addSubView() {
        view.addSubview(safetyContainer)
        view.addSubview(startButton)
    }
    
    func constraintsMaker() {
        safetyContainer.snp.makeConstraints { (maker) in
            if #available(iOS 11.0, *) {
                maker.top.bottom.leading.trailing.equalTo(view.safeAreaLayoutGuide)
            } else {
                // Fallback on earlier versions
                maker.top.bottom.leading.trailing.equalToSuperview()
            }
        }
        
        startButton.snp.makeConstraints { (maker) in
            maker.center.equalToSuperview()
            maker.width.equalTo(100)
            maker.height.equalTo(40)
        }
    }
    
    func additionalLayout() {
        startButton.setTitle("START", for: .normal)
        startButton.setTitleColor(.black, for: .normal)
        startButton.layer.borderColor = UIColor.black.cgColor
        startButton.layer.borderWidth = 1.0
        startButton.clipsToBounds = true
        startButton.layer.cornerRadius = 20.0
    }
    
    func ManageBackGround() {
        safetyContainer.backgroundColor = .red
    }
    
    @objc func startButtonPressed() {
        let manager = SampleAppManager()
        manager.startButtonPressed(navigationController: navigationController)
    }
    
    func addActions() {
        startButton.addTarget(self, action: #selector(startButtonPressed), for: .touchUpInside)
    }
}
